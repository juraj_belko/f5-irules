# Server nevie či request, ktory mu pride, ma byt https alebo http. Pretože mu autorita nehovori pomocou hlavicky X-Forwarded-Proto. Toto osetrenie treba nastavit na loadbalanceri. Staci mu nastavit X-Forwarded-Proto https. Takto aplikacia, ktora generuje linky pre frontend bude vediet kde ma pouzit https.
#
# Riesenie:
# V loadbalanceri je potrebne pridat/nastavit:
# X-Forwarded-Proto https
# RemoteIPHeader X-Forwarded-For

# * 20200910
# Juraj Belko

when HTTP_REQUEST {
    # X-Forwarded-Proto
    if { [HTTP::header exists "X-Forwarded-Proto"] } {
        HTTP::header replace "X-Forwarded-Proto" "https"
    } else {
        HTTP::header insert "X-Forwarded-Proto" "https"
    }
    
    # X-Forwarded-For
    if { [HTTP::header exists "X-Forwarded-For"] } {
        HTTP::header replace "X-Forwarded-For" "[IP::remote_addr]"
    } else {
        HTTP::header insert "X-Forwarded-For" "[IP::remote_addr]"
    }
}