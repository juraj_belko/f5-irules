#
# iRule for monitoring HTTP traffic requests, response times and infra proxy chain latency.
#
# by Juraj Belko <juraj_belko@sucks.sk>
# 20190601
# 20190606
# 20190607
# 20190611
# 20190716
#  - label rework
#  - sort otuput for human reading,
#  - adding sufixes for better reading
#  - add comments and help
# 20200317
#  - X-B3-TraceId header added
# 20200902
#  - added SSL cipher name, its version, and the number of secret bits used
#
# based on work
# performance logging - Tim Riker <Tim@Rikers.org>

# Loging will be done to remote syslog and to local ltm file.

# Init Steps:
# Create pool SYSLOG_POOL, with members allowed to accept syslog

# Testing Sylog
# echo 'mesage text' | nc -v -u -w0 10.2.200.170 515

# Options
when RULE_INIT {  
	# Log debug to /var/log/ltm? 1=yes, 0=no
	set static::payload_dbg 1

	# Limit payload collection to 5Mb
	#set static::max_collect_len 5368709

	# Max characters to log locally (must be less than 1024 bytes)
	# https://devcentral.f5.com/wiki/iRules.log.ashx
	set static::max_chars 900

	# static mappings
	# hostname
 	set static::hostname [info hostname]

 	# HSL pool - SYSLOG_POOL
	set static::syslog_pool "SYSLOG_POOL"
}

# Logging function
# Argument will be sent to log server and log file
proc slog msg {
    # Open HSL connection
	set hsl [HSL::open -proto UDP -pool $static::syslog_pool]
	# Send data over HSL
    HSL::send $hsl $msg
    # Log locally
    if {$static::payload_dbg}{log local0. $msg}
}

# Logging func V2
# first argument is time
# second argument is logarray
proc slog2 {time msg} {
    # Add msg to log array
    upvar 1 $msg logarray
    # Open HSL connection
	set hsl [HSL::open -proto UDP -pool $static::syslog_pool]

	# Format output PREFIX here
	set output "[clock format [string range $time 0 end-3] -gmt 1 -format %Y-%m-%dT%H:%M:%S.[string range $time end-2 end]+00:00] ${static::hostname} perfmon -"

	# Sort Array for consitnet output and handle spaces
	# Append to output
	foreach key [lsort [array names logarray]] {
        if { ($logarray($key) contains " ") } {
            append output " $key=\"$logarray($key)\""
        } else {
            append output " $key=$logarray($key)"
        }
    }

    # Send output data over HSL - a.k.a. syslog
    HSL::send $hsl $output
    # Log locally
    if {$static::payload_dbg}{log local0. $output}
}

when CLIENT_ACCEPTED {
    ##call slog "CLIENT_ACCEPTED event"

    # Calculate times, track request time
    # TCP start time in ms
    set tcp_start_time [clock clicks -milliseconds]
    # Clinet IP address
    set log(tcp_client_addr) [IP::client_addr]
    # Client source port
    set log(tcp_client_port) [TCP::client_port]
    # CPU usage
    set log(stat_cpu_5sec) [cpu usage 5secs]
    # Virtual Name
    #set log(stat_virtual_name) [virtual name]
    # remove "/Common/" from virtual name
    set log(stat_virtual_name) [string map -nocase {"/Common/" ""} [virtual name]]
    # VIP address
    #set log(vip_addr) [IP::server_addr]
    # VIP port
    #set log(vip_port) [IP::server_port]
}

when CLIENTSSL_HANDSHAKE {
    ##call slog "CLIENTSSL_HANDSHAKE event"

    # SSL cipher name
    set log(ssl_cipher_name) [SSL::cipher name]
    # SSL cipher version
    set log(ssl_cipher_version) [SSL::cipher version]
    # SSL cipher bits
    set log(ssl_cipher_bits) [SSL::cipher bits]
}

when HTTP_REQUEST {
    ##call slog "HTTP_REQUEST event"

    # Request start time in ms
    set http_request_time [clock clicks -milliseconds]
    # HTTP Host Header
    set log(http_host) [HTTP::host]
    # HTTP URI
    set log(http_uri) [HTTP::uri]
    # HTTP Method
    set log(http_method) [HTTP::method]
    # Rquest number
    set log(http_request_num) [HTTP::request_num]
    # HTTP version
    set log(http_version) [HTTP::version]

    # Log interesting HTTP Headers, if exists
    # Name all headers of interest here
    foreach {header} {"connection" "content-length" "keep-alive" "last-modified" "policy-cn" "referer" "user-agent" "x-forwarded-for" "x-forwarded-proto" "x-forwarded-scheme" "X-B3-TraceId" } {
        if { [HTTP::header exists $header] } {
            set log(http_req-$header) [HTTP::header $header]
        }
    }
}

when ACCESS_SESSION_STARTED {
    ##call slog "ACCESS_SESSION_STARTED event"

    # Log User name and SSID in this event - due to APM cert auth
    # APM SSID
    set log(apm_session_sid) [ACCESS::session sid]
    # APM username custom vs generic
    set log(apm_session_username) [ACCESS::session data get session.custom.erecept.username]
    #set log(session_username) [ACCESS::session data get session.logon.last.username]
}

when LB_SELECTED {
    ##call slog "LB_SELECT event"

    # Time when backend is choosen from pool in ms
    set lb_selected_time [clock clicks -milliseconds]
    # Backend address
    set log(tcp_backend_addr) [LB::server addr]
    # Backend dst port
    set log(tcp_backend_port) [LB::server port]
    # Pool name - disabled for cleaner output
    # set log(pool) [LB::server pool]
}

when SERVER_CONNECTED {
    ##call slog "SERVER_CONNECTED event"

    # Log time between received tcp connection from client to time taken to select and connect to pool member
    set log(time_connection_server) [expr {[clock clicks -milliseconds] - $lb_selected_time}]
    # SNAT address - for better troubleshooting in network environment
    set log(tcp_snat_addr) [IP::local_addr]
    # SNAT port - disabled for cleaner output
    #set log(snat_port) [TCP::local_port]
}

when HTTP_REQUEST_SEND {
    ##call slog "HTTP_REQUEST_SEND event"

    # Save Time For later use and calculations
    set http_request_send_time [clock clicks -milliseconds]
}

when HTTP_RESPONSE {
    ##call slog "HTTP_RESPONSE event"

    # Time between http request from client and http response, before sending response back to client
    set log(time_received_response) [expr {[clock clicks -milliseconds] - $http_request_send_time}]
    # HTTP status 
    set log(http_status) [HTTP::status]

    foreach {header} {"cache-control" "connection" "content-length" "content-security-policy" "keep-alive" "last-modified" "server"} {
        if { [HTTP::header exists $header] } {
            set log(http_res-$header) [HTTP::header $header]
        }
    }
}

when HTTP_RESPONSE_RELEASE {
    ##call slog "HTTP_RESPONSE_RELEASE event"

    # This is the place where we want to log everything waht was collected
    # Time how long it take from client request to serve response back to client from backend
    set log(time_response) "[expr {[clock clicks -milliseconds] - $http_request_time}]"
    set log(stat_event) HTTP_RESPONSE_RELEASE

    # Send Logs to HSL
    call slog2 $http_request_time log
}

when CLIENT_CLOSED {
    ##call slog "CLIENT_CLOSED event"

    if { not ([info exists log(time_response)])} {
        if { [info exists http_request_time] } {
            # never called HTTP_REQUEST?
            set log(time_response) "[expr {[clock clicks -milliseconds] - $http_request_time}]"
        }
        set log(tcp_time) "[expr {[clock clicks -milliseconds] - $tcp_start_time}]"
        set log(stat_event) CLIENT_CLOSED

        call slog2 $tcp_start_time log
    }    
}