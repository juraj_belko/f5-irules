#
# Juraj Belko
#
# https://support.f5.com/csp/article/K14784
# https://devcentral.f5.com/s/articles/encrypting-cookies
#
# Encryption for all cookies.
# Unfortunately, profile need to name cookie names in advance:
# 	If you want to specify more than one cookie for the BIG-IP LTM system to encrypt, separate the cookie names with a space character.
# 	For BIG-IP persistent cookies, the default cookie name is BIGipServer<pool-name>.
#	For example:
#	BIGipServerhttp-pool
#
# This want to be slightly universal solutions for encryptions of defaults F5 cookies
#
# Changelog:
#  * 20210824 - init
#

priority 10
when CLIENT_ACCEPTED {
	# Set cookie encryption passphrase
	set encryption_passphrase "seckyceckyven"
}

# Encrypt All cookies on the way to client 
priority 10
when HTTP_RESPONSE {
	# For all cookies with name starting with "BIGipServer"
	foreach cookie_name [HTTP::cookie names] {
		if {$cookie_name starts_with "BIGipServer"} {
			HTTP::cookie encrypt $cookie_name $encryption_passphrase
		}
	}
}

# Decrypt encrypted cookies on income
priority 10
when HTTP_REQUEST {
	# For all cookies with name starting with "BIGipServer"
	foreach cookie_name [HTTP::cookie names] {
		if {$cookie_name starts_with "BIGipServer"} {
			set decrypted [HTTP::cookie decrypt $cookie_name $encryption_passphrase]
			if { ($decrypted eq "") } {
				# Cookie wasn't encrypted, delete it
				HTTP::cookie remove $cookie_name
			}
		}
	}
}